/*
1.Опишіть своїми словами що таке Document Object Model (DOM)
DOM- это объектная модель документа, передает структуру страницы, состоит из объектов, 
которые можно менять

2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML- выдает текстовое содержание элемента вместе с разметкой HTML
innerText - передает текстовое содержимое , не включает в себя элементы HTML

3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
с помощьюю метода elem.querySelector(css)- возвращает первый элемент , который отвечает селектроу, 
или elem.querySelectorAll()- возвр. все совпадающие эл-ты.   Второй вариант - с помощью 
метода getElementBy, исп-ся при поиске по тегу, классу, устаревший вариант, тк querySelector 
более эффективный



Завдання
Код для завдань лежить в папці project.

1.Знайти всі параграфи на сторінці та встановити колір фону #ff0000

2.Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести
в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

3.Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
This is a paragraph

4.Отримати елементи <li> вкладені в елемент із класом main-header і вивести їх у консоль.
Кожному з елементів присвоїти новий клас nav-item.

5.Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

*/

// 1.
const p = document.querySelectorAll ('p');
console.log(p);
document.body.style.background = '#ff0000';

// 2.
const optionslist = document.getElementById('optionsList');
console.log(optionslist);
console.log(optionslist.parentNode);
console.log(optionslist.childNodes);

optionslist.childNodes.forEach((el) => console.log(el.nodeName));
optionslist.childNodes.forEach ((el) => console.log(el.nodeType));

// 3.
const elTestParagraph = document.getElementById('testParagraph');
elTestParagraph.textContent= 'This is a paragraph';
console.log(elTestParagraph);

// 4.
const mainHeaderLi = document.querySelectorAll('.main-header li');
mainHeaderLi.forEach(el => el.classList.add('nav-item'));
console.log(mainHeaderLi);

// 5.
const sectionTitleClass = document.querySelectorAll('.section-title');
sectionTitleClass.forEach(el=> el.classList.remove('section-title'));
console.log(sectionTitleClass);